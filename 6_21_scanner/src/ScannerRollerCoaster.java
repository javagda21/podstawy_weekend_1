import java.util.Scanner;

public class ScannerRollerCoaster {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie!");
        String imie = scanner.next();

        System.out.println("Cześć " + imie);
        System.out.println("Podaj wiek:");
        int wiek = scanner.nextInt();

        System.out.println("Podaj wzrost:");
        int wzrost = scanner.nextInt();

        System.out.println("Podaj wagę:");
        int waga = scanner.nextInt();

        if(wiek > 10 && wiek < 80 && wzrost > 150 && wzrost < 220 && waga < 180){
            System.out.println("Siema, wchodzisz!");
        }
        if(!(wiek > 10 && wiek < 80)){
            System.out.println("Niepoprawny wiek!");
        }
        if(!(wzrost > 150 && wzrost < 220)){
            System.out.println("Nieporawny wzrost!");
        }
        if(!(waga < 180)){
            System.out.println("Niepoprawna waga!");
        }


    }
}
