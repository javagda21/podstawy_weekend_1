import java.util.Scanner;

public class ScannerRollerCoaster_2 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie!");
        String imie = scanner.next();

        System.out.println("Cześć " + imie);
        System.out.println("Podaj wiek:");
        int wiek = scanner.nextInt();
        if (!(wiek > 10 && wiek < 80)) {
            System.out.println("Niepoprawny wiek!");
            // kończy się metoda
            return; // program się kończy
        }

        System.out.println("Podaj wzrost:");
        int wzrost = scanner.nextInt();
        if (!(wzrost > 150 && wzrost < 220)) {
            System.out.println("Nieporawny wzrost!");
            return; // program się kończy
        }

        System.out.println("Podaj wagę:");
        int waga = scanner.nextInt();
        if (!(waga < 180)) {
            System.out.println("Niepoprawna waga!");
            return; // program się kończy
        }

        //ostateczna weryfikajca
        System.out.println("Siema, wchodzisz!");
    }
}
