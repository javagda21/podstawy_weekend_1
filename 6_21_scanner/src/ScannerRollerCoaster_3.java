import java.util.Scanner;

public class ScannerRollerCoaster_3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj imie!");
        String imie = scanner.next();

        System.out.println("Cześć " + imie);
        System.out.println("Podaj wiek:");
        int wiek = scanner.nextInt();

        System.out.println("Podaj wzrost:");
        int wzrost = scanner.nextInt();

        System.out.println("Podaj wagę:");
        int waga = scanner.nextInt();

        if (wiek > 10 && wiek < 80) {
            if (wzrost > 150 && wzrost < 220) {
                if (waga < 180) {
                    System.out.println("Siema, wchodzisz!");
                } else {
                    System.out.println("Jesteś zbyt ciężki!");
                }
            } else if (wzrost <= 150) {
                System.out.println("Jesteś zbyt niski!");
            } else {
                System.out.println("Jesteś zbyt wysoki!");
            }
        } else if (wiek <= 10) {
            System.out.println("Jesteś zbyt młody!");
        } else{
            System.out.println("Jesteś zbyt stary");
        }
    }
}
