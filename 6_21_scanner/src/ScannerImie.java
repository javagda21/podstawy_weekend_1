import java.util.Scanner;

public class ScannerImie {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String imie;
//        imie = scanner.next(); // wczyta słowo [ wszystko do spacji ]
        // starajmy się nie mieszać instrukcji next() z nextLine().
        System.out.println("Podaj zmienną");
        String zmienna1 = scanner.next(); // słowo
        System.out.println("Podaj resztę linii");
        imie = scanner.nextLine(); // \n

        System.out.println("Zmienna: " + zmienna1);
        System.out.println("Imie: " + imie);

        double zmienna_double = scanner.nextDouble();
    }
}
