public class IfRollercoaster {
    public static void main(String[] args) {
        int wzrost = 180;
        int waga = 86;

        if (wzrost > 150 && waga < 180) {
            System.out.println("Wchodzisz !");
        } else {
            System.out.println("Sorry, nie możesz wejść");
        }
    }
}
