public class SwitchMain {
    public static void main(String[] args) {
        int wartoscLiczbowa = 5;

        switch (wartoscLiczbowa) {
            case 5:
                System.out.println("Komunikat 1");
            case 6:
                System.out.println("Komunikat 6");
            case 7:
                System.out.println("Komunikat 7");
                break;
            default:
                System.out.println("Komunikat 6");
                break;
        }

        System.out.println("Komunikat 3");
    }
}
