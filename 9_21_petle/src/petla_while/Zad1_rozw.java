package petla_while;

public class Zad1_rozw {
    public static void main(String[] args) {
        // d
        System.out.println("Podpunkt d/while");
        int d = 1;
        while (d <= 100) {
            if (d % 3 == 0) {
                System.out.println(d);
            }
            d++;
        }

        System.out.println("Podpunkt e/while");
        int e = 30;
        while (e <= 300) {
            if (e % 3 == 0 && e % 5 == 0) {
                System.out.println(e);
            }
            e++;
        }

        System.out.println("Podpunkt f/while");
        int f = -300;
        while (f <= 300) {
            if (f % 2 != 0) {
                System.out.println(f);
            }
            f++;
        }

        System.out.println("Podpunkt g/while");
        int g = -100;
        while (g <= 100) {
            if (g % 2 == 0) {
                System.out.println(g);
            }
            g++;
        }

        System.out.println("Podpunkt h/while");
        char h = 'a';
        while (h <= 'z') {
            System.out.println(h);
            h++;
        }

        System.out.println("Podpunkt k/while");
        char k = 'b';
        while (k <= 'z') {
            if(k%5==0) {
                System.out.println(k);
            }
            k+=2;
        }


    }
}
