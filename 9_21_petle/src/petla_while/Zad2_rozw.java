package petla_while;

public class Zad2_rozw {
    public static void main(String[] args) {
        System.out.println("Podpunkt A.");
        for (int i = -200; i > -1000; i--) {
            System.out.println(i);
        }

        int a = -200;
        while (a > -1000) {
            System.out.println(a);
            a--;
        }

        System.out.println("Podpunkt B.");
        for (int i = 1000; i < 10000; i++) {
            System.out.print(i + " ");
            if (i % 1000 == 0) {
                System.out.println();
            }
        }

        int b = 1000;
        while (b < 10000) {
            System.out.println(b);
            if (b % 1000 == 0) {
                System.out.println();
            }
            b++;
        }

        System.out.println("Podpunkt C.");
        for (int wiersz = 1; wiersz <= 5; wiersz++) {
            for (int kolumna = 1; kolumna <= 10; kolumna++) {
                System.out.print(wiersz * kolumna + " ");
            }
            System.out.println();
        }

        System.out.println();
        System.out.println();
        int wiersz = 1;
        while (wiersz <= 5) {
            int kolumna = 1;
            while (kolumna <= 10) {
                System.out.print(wiersz * kolumna + " ");
                kolumna++;
            }
            System.out.println();
            wiersz++;
        }

    }
}
