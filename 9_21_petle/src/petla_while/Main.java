package petla_while;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int suma = 0;
        for (int i = 0; i < 100; i++) {

            suma += i;
            System.out.println(i + ". Hello!");
        }
        System.out.println(suma);

        //#############################################################
        //inicjalizacja
        int i = 0;
        while (i < 100) {

            System.out.println(i + ". Hello!");

            i++; // inkrementacja - po każdym obiegu
        }
        System.out.println(i);

        //#############################################################
        // kiedy wiemy że zbiór instrukcji musi wykonać się co najmniej raz
        // to powinniśmy zastosować do while.
        Scanner scanner = new Scanner(System.in);
        int liczba;
        do {
            liczba = scanner.nextInt();

        }while (liczba!=0);



        // -30  -> 5000
        int j = -30;
        while (j < 5000) {
            System.out.println(j + ". Hello!");

            j+=2;
        }

        j = -30;
        do{

        }while (j<5000);

    }
}
