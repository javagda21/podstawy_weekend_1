package petla_do_while;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int liczba = scanner.nextInt();
        while (liczba != 0) {
            liczba = scanner.nextInt();
        }

        // kiedy wiemy że zbiór instrukcji musi wykonać się co najmniej raz
        // to powinniśmy zastosować do while.
        do {
            liczba = scanner.nextInt();

        }while (liczba!=0);
    }
}
