public class Operatory {
    public static void main(String[] args) {
        System.out.println(false == false);
        System.out.println(false != true);
        System.out.println(!true);
        System.out.println(2 > 4);
        System.out.println(3 > 5);
        System.out.println(3 == 3 && 3 == 4);
        System.out.println(3 != 5 || 3 == 5);
        System.out.println((2 + 4) > (1 + 3));
        System.out.println("cos".equals("cos"));
        System.out.println("cos" == "cos");

        // == - porównanie
        // equals - string - porównanie wartości (treści ciągu znaków)

        String zm1 = new String("cos");
        String zm2 = new String("cos");
        System.out.println("Sprawdzenie: ");
        System.out.println(zm1 == zm2);

        short tysiac = 1000;
        byte tysiak = (byte) tysiac;
        System.out.println(tysiac);
        System.out.println(tysiak);
    }
}
