public class Operacje {
    // 2 + 3 (int)
    // 2.0 + 3 (double)
    // 3 + (2.0 +1 ) (double) 6.0
    // 3.0 + (3 / 2) (double) 4.0
    public static void main(String[] args) {
        System.out.println("1." + (2 + 3));
        System.out.println("2." + (2 - 4));
        System.out.println("3." + (5 / 2));
        System.out.println("4." + (5.0 / 2));
        System.out.println("5." + (5 / 2.0));
        System.out.println("6." + (5.0 / 2.0));
        System.out.println("7." + (100L - 10));
        System.out.println("8." + (2f - 3));
        System.out.println("9." + (5f / 2));
        System.out.println("10." + (5d / 2));
        System.out.println("11." + ('A' + 2));
        System.out.println("12." + ('a' + 2));
        System.out.println("13." + ("a" + 2));
        System.out.println("14." + ("a" + "b"));
        System.out.println("15." + ('a' + 'b'));
        System.out.println("16." + ("a" + 'b'));
        System.out.println("17." + ("a" + 'b' + 3));
        System.out.println("18." + ('b' + 3 + "a"));
        System.out.println("19." + (9 % 4)); // 1

        int a = 5;

//        a = a + 20;
//        a += 20;
//        a /= 20;
//        a *= 20;
//        a -= 20;
//        int b = a++; // b = 5, a = 6 // postinkrementacja
        int b = ++a; // b = 6, a = 6 //preinkrementacja
        System.out.println(b);
        // w b jest

    }
}
