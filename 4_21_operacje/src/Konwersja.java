public class Konwersja {
    public static void main(String[] args) {
        // A.
        short zmienna_short_a = 50;
        int zmienna_int_a = zmienna_short_a;

        // B.
        short zmienna_short_b = 5000;
        long zmienna_long_b = zmienna_short_b;

        // C.
        int zmienna_int_c = 2;
        float zmienna_float_c = zmienna_int_c;

        // D.
        int zmienna_int_d = 5;
        double zmienna_double_d = zmienna_int_d;

        // E.
        long zmienna_long_e = 50000000000000L;
        int zmienna_int_e = (int) zmienna_long_e;

        // F.
        short zmienna_short_f = 5324;
        byte zmienna_byte_f = (byte) zmienna_short_f;

        // G.
        char zmienna_char_g = '5';
        int zmienna_int_g = zmienna_char_g;
    }
}
