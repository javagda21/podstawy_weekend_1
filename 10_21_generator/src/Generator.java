import java.util.Random;
import java.util.Scanner;

public class Generator {
    public static void main(String[] args) {
//        Scanner scanner = new Scanner(System.in);

        Random generator = new Random();
        int liczba = generator.nextInt(); // zakres int -2xxxxxxxxx - +2xxxxxxxxx
        System.out.println(liczba);

        int liczbaZZakresu = generator.nextInt(100); //0-99
        System.out.println(liczbaZZakresu);

        int liczbaZZakresuInnego = generator.nextInt(31) + 20; //20-50
        System.out.println(liczbaZZakresuInnego);

        int liczbaZZakresuInnegoUjemnego = generator.nextInt(101) - 50; //-50 do 50
        System.out.println(liczbaZZakresuInnegoUjemnego);

        // podpunkt 1.
        for (int i = 0; i < 100; i++) {
//            int liczbaWylosowana = generator.nextInt(51);
//            System.out.println(liczbaWylosowana);

            System.out.println(generator.nextInt(51));
        }

        // podpunkt 2.
        for (int i = 0; i < 100; i++) {
//            int liczbaWylosowana = generator.nextInt(901) + 100;
//            System.out.println(liczbaWylosowana);

            System.out.println((generator.nextInt(901) + 100) + " ");
        }

        // podpunkt 3.
        for (int i = 0; i < 1000; i++) {
//            int liczbaWylosowana = generator.nextInt(5201) - 200;
//            System.out.println(liczbaWylosowana);

            System.out.println((generator.nextInt(5201) - 200) + " ");
        }

        System.out.println();
        System.out.println();
        // podpunkt 4.
        int licznikPiątek = 0;
        for (int i = 0; i < 10; i++) {

            int liczbaWylosowana = generator.nextInt(11);
            System.out.print(liczbaWylosowana + " ");

            if (liczbaWylosowana == 5) {
                licznikPiątek++;
            }
        }
        System.out.println();
        System.out.println("Wystąpień 5: " + licznikPiątek);

        // podpunkt 5.
        int licznik_2 = 0;
        int licznik_4 = 0;
        for (int i = 0; i < 20; i++) {
            int liczbaL = generator.nextInt(5) + 1;

            if (liczbaL == 2) { // 2
                licznik_2++;
            } else if (liczbaL == 4) {
                licznik_4++;
            }
        }
        System.out.println("\n"); // wypisanie dwóch nowych linii. Jedna z println, druga z \n
        System.out.println("Licznik 2: " + licznik_2);
        System.out.println("Licznik 4: " + licznik_4);

    }

}
