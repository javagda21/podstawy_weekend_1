import java.util.Random;
import java.util.Scanner;

public class MathFunctions {
    public static void main(String[] args) {
        Random random = new Random();
        double wynik = random.nextDouble() * 200; // 0.0 - 200.0
        double wynik2 = random.nextDouble() * 180 + 20.0; // 20.0 - 200.0

        double wynikMath = Math.random(); // 0.0 - 1.0 (exclusive)

        double zaokraglenieWGore = Math.ceil(wynikMath);
        double zaokraglenieWDol = Math.floor(wynikMath);
        double pierwiastek = Math.sqrt(wynik);
        double potega = Math.pow(wynik, 5); // zmienna wynik podniesiona do potęgi 5
        double wartoscBezwzględna = Math.abs(wynik);
        // dla -5 -> 5
        // dla -2.323 -> 2.323
        // dla 1 -> 1

        // Podpunkt 9
        for (int i = 0; i < 5; i++) {
            double liczba = random.nextDouble() * 100 + 100;
            System.out.println(liczba);
        }

        // Podpunkt 10
        for (int i = 0; i < 10; i++) {
            double liczba = random.nextDouble() * 30 - 10; // -10 do 20
            System.out.println(liczba);
        }

        // Podpunkt 11
        Scanner scanner = new Scanner(System.in);
        double liczbaWybrana;
        do {
            System.out.println("Podaj liczbę:");
            liczbaWybrana = scanner.nextDouble();
        } while (liczbaWybrana > 5.3 || liczbaWybrana < -5.3);

        // Wylosuj 5 liczb zmiennoprzecinkowych z zakresu -100 do 100, wypisz ich zaokrąglenia w górę i w dół w linii.
        //np. wylosowano 2.3123, wypisz linię:
        //Liczba 2.3123, ceil: 3.0, floor: 2.0, abs: 2.3123, sqrt: xxx, pow: xxx

        // Podpunkt 12
        for (int i = 0; i < 5; i++) {
            double liczba = random.nextDouble() * 200 - 100;

            double zaokraglenieWGoreL = Math.ceil(liczba);
            double zaokraglenieWDolL = Math.floor(liczba);
            double wartBezwzgl = Math.abs(liczba);
            double potegaL2 = Math.pow(liczba, 2);
            double pierwiastekzL = Math.sqrt(liczba);

            System.out.println("Liczba: " + liczba + ", ceil: " + zaokraglenieWGoreL + ", floor: " + zaokraglenieWDolL
                    + ", abs: " + wartBezwzgl + ", sqrt: " + pierwiastekzL + ", pow: " + potegaL2);
        }
    }
}
