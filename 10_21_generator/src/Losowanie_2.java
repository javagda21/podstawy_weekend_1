import java.util.Random;
import java.util.Scanner;

public class Losowanie_2 {
    public static void main(String[] args) {
        // Podpunkt 6
        Random random = new Random();

        int liczbaWierszy = random.nextInt(11) + 20;
        int liczbaKolumn = random.nextInt(4) + 3; // 3, 4, 5, 6

        for (int wiersz = 1; wiersz <= liczbaWierszy; wiersz++) {
            for (int kolumna = 1; kolumna <= liczbaKolumn; kolumna++) {
                System.out.print(kolumna * wiersz + " ");
            }
            System.out.println();
        }

        // Podpunkt 7
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj liczbę X:");
        int wybrana = scanner.nextInt();

        boolean czyZnaleziono = false;
        for (int i = 0; i < 5; i++) {
            int wylosowana = random.nextInt(49) + 1;

            if (wylosowana == wybrana) {
                czyZnaleziono = true;
                System.out.println("Trafiłeś");
            } else {
                System.out.println("Nie trafiłeś");
            }
        }
        if (czyZnaleziono) {
            System.out.println("W zbiorze jest Twoja liczba");
        } else {
            System.out.println("Nie trafiłeś!");
        }

        // Podpunkt 8.

        // podejście 1:
        int X;
        do {
            System.out.println("Podaj liczbę z zakresu 10 - 100:");
            X = scanner.nextInt();
        } while (X < 10 || X > 100);

        // podejście 2:
        do {
            System.out.println("Podaj liczbę z zakresu 10 - 100:");
            X = scanner.nextInt();
            if (X > 10 && X < 100) {
                // jest w zakresie (użytkownik podał poprawną liczbę).
                break;
            }
        } while (true);


    }
}
