package zad4;

public class Main {
    public static void main(String[] args) {
        // losowe wartości dla 3 zmiennych
        boolean jest_cieplo = true;
        boolean wieje_wiatr = true;
        boolean swieci_slonce = true;

        boolean ubieram_sie_cieplo = !jest_cieplo || wieje_wiatr;
        boolean biore_parasol = !swieci_slonce && !wieje_wiatr;
        boolean ubieram_kurtke = wieje_wiatr && !swieci_slonce && !jest_cieplo;

        System.out.println("Ubieram sie cieplo: " + ubieram_sie_cieplo);
        System.out.println("Biore parasol: " + biore_parasol);
        System.out.println("Ubieram kurtke: " + ubieram_kurtke);

//        if(!jest_cieplo || wieje_wiatr){
//            ubieram_sie_cieplo  = true;
//        }else{
//            ubieram_sie_cieplo = false;
//        }
    }
}
