package zad5;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj temperatorę w stopniach Celsjusza:");
        double temperaturaCelsjusza = scanner.nextDouble();

        double stopnieFarenheita = 1.8 * temperaturaCelsjusza + 32.0;
        System.out.println("Temperatura Farenheit'a: " + stopnieFarenheita);
        System.out.println(String.format("Temperatura Farenheit'a: %.1f", stopnieFarenheita));


        System.out.println(String.format("Temperatura Farenheit'a: %.1f", 1.8 * temperaturaCelsjusza + 32.0));
    }
}
