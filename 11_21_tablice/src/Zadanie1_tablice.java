import java.util.Random;

public class Zadanie1_tablice {
    public static void main(String[] args) {
        int[] tablica = new int[10];

        Random generator = new Random();

        for (int i = 0; i < tablica.length; i++) {
            // wygenerowanie n liczb z zakresu -10 - 10
            // wstawienie elementów w tablicę
            tablica[i] = generator.nextInt(21) - 10;
            System.out.print(tablica[i] + ", ");
        }
        System.out.println();

        // wypisanie elementów po kolei
        for (int i = 0; i < tablica.length; i++) {
            System.out.print(tablica[i] + ", ");
        }
        System.out.println();

//        int min = podajMinimum(tablica);
        int max = podajMaximum(tablica);

        System.out.println(podajMinimum(tablica));
        System.out.println(max);


        double suma = getSuma(tablica);

        System.out.println(suma);
        double średnia = (suma/tablica.length);
        System.out.println("Wynik: " + średnia);

        int wynikMetody = metodaPodajMinimum(5, 20);
        System.out.println(wynikMetody);
    }

    public static double getSuma(int[] tablica) {
        double suma = 0;
        // sumowanie elementów w tablicy
        for (int i = 0; i < tablica.length; i++) {
            suma += tablica[i];
        }
        return suma;
    }

    public static int podajMinimum(int[] olaboga){
        // deklaruje min = olaboga[0] - pierwszy element tablicy
        int min = olaboga[0];
        // iteruję od drugiego elementu tablicy (pierwszego nie muszę już sprawdzać
        // bo jest przypisany do zmiennej 'min' jedną linię wyżej
        for (int i = 1; i < olaboga.length; i++) {
            // sprawdzenie czy moje obecne min jest mniejsze od elementu i'tego
            if(olaboga[i] < min){
                min = olaboga[i];
            }
        }

        return min;
    }

    public static int podajMaximum(int[] tablica){
        // deklaruje min = tablica[0] - pierwszy element tablicy
        int max = tablica[0];
        // iteruję od drugiego elementu tablicy (pierwszego nie muszę już sprawdzać
        // bo jest przypisany do zmiennej 'min' jedną linię wyżej
        for (int i = 1; i < tablica.length; i++) {
            // sprawdzenie czy moje obecne max jest większe od elementu i'tego
            if(tablica[i] > max){
                max = tablica[i];
            }
        }

        return max;
    }

    public static int metodaPodajMinimum(int a, int b){
        if(a < b) {
            return a;
        }
        return b;
    }
}
