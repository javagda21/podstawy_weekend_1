public class TabliceZadanie {
    public static void main(String[] args) {
        int[] tablica = new int[]{1, 3, 5, 10};

        System.out.println(tablica[1]);
        System.out.println(tablica[3]);

//        System.out.println(tablica[4]); // < błąd

        for (int i = 0; i < tablica.length; i++) {
            System.out.println(tablica[i]);
        }

        // o parzystym indeksie
        for (int i = 0; i < tablica.length; i++) {
            if (i % 2 == 0) {
                System.out.println(tablica[i]);
            }
        }

        // o parzystej wartości
        for (int i = 0; i < tablica.length; i++) {
            if (tablica[i] % 2 == 0) {
                System.out.println(tablica[i]);
            }
        }

        for (int i = tablica.length - 1; i >= 0; i++) {
            System.out.println(tablica[i]);
        }

    }
}
