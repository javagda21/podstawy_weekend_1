import java.util.Arrays;

public class TablicaString {
    public static void main(String[] args) {
//        String[] tablica2;
//        System.out.println(tablica2); // < - nie jest możliwe

        String[] tablica = new String[] {"a", "b", "null", null, null, "1"};
        System.out.println(tablica); // < - wyjdą krzaki
        System.out.println(Arrays.toString(tablica)); // < wypisze elementy

        System.out.println(tablica[1]); // < - wypisuje wartość elementu pod indeksem 1 (2 element)

        System.out.println(tablica.length); // < - wypisze długość tablicy
        //                                       nie mówi ile jest niepustych elementów
        for (int i = 0; i < tablica.length; i++) { // jeśli rozmiar wskazuje na 5
            System.out.println(tablica[i]);
        }

        // wypisanie tablicy od końca
        for (int i = tablica.length - 1; i >= 0; i--) {
            System.out.println(tablica[i]);
        }

        // tablica[i] - element na itej pozyjci
        // i - indeks / iterator

    }

    public static void metoda(){
        System.out.println("Halo!");
    }
}
