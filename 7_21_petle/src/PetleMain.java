public class PetleMain {
    public static void main(String[] args) {

        // int i = 0 - wykona się 1 raz zanim pętla wystartuje
        //  i < 10 - warunek który sprawdza się przed każdym wejściem w blok instrukcji
        // i++ - inkrementacja która wykonuje się po każdym wykonaniu bloku instrukcji { }
        for (int i = 0; i < 15; i++) { // 0, 1, 2.... 14
            System.out.println(i + ". komunikat");
        }

        System.out.println("\nPodpunkt A.");
        for (int i = 1; i < 101; i++) {
            System.out.println(i);
        }

        System.out.println("\nPodpunkt B.");
        for (int i = 1000; i < 1020; i++) {
            System.out.print(i + ", ");
        }

        System.out.println("\nPodpunkt C.");
        for (int i = -30; i <= 1000; i++) {
            if (i % 5 == 0) {
                System.out.println(i);
            }
        }

        System.out.println("\nPodpunkt D.");
        for (int i = 1; i < 101; i++) {
            if (i % 3 == 0) {
                System.out.println(i);
            }
        }

        System.out.println("\nPodpunkt E.");
        for (int i = 30; i <= 300; i++) {
            if (i % 3 == 0 && i % 5 == 0) {
                System.out.println(i);
            }
        }

        System.out.println("\nPodpunkt F.");
        for (int i = -300; i <= 300; i++) {
            if (i % 2 == 1) {
                System.out.print(i + ";");
            }
        }

        System.out.println("\nPodpunkt G.");
        for (int i = -100; i <= 100; i++) {
            if (i % 2 == 0) {
                System.out.print(i + ";");
            }
        }

        System.out.println("\nPodpunkt H.");
        for (char i = 'a'; i <= 'z'; i++) {
            System.out.println(i);
        }

        System.out.println("\nPodpunkt I.");
        for (char i = 'A'; i <= 'Z'; i++) {
            System.out.println(i);
        }

        System.out.println("\nPodpunkt J.");
        for (char i = 'A'; i <= 'Z'; i += 2) {
            System.out.println(i);
        }

        System.out.println("\nPodpunkt K.");
        for (char i = 'b'; i <= 'z'; i += 2) {
            if (i % 5 == 0) {
                System.out.println(i);
            }
        }

        System.out.println("\nPodpunkt L.");
        for (int i = 0; i < 100; i++) {
            System.out.println((i + 1) + ". Hello World");
        }
    }
}
