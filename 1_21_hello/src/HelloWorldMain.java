public class HelloWorldMain {
    // komentarz
    // 1. sygnatura metody -
    // public static void main(String[] args)
    // 2. ciało metody -
    // { }
    public static void main(String[] args) {
        // wypisz tekst oraz zakończ linię - postaw enter na końcu
        System.out.println("Hello World!");

        // wypisz tekst
        System.out.print("Hello World!");
        System.out.print("Hello World!");
        System.out.println("sout");
    }
}
