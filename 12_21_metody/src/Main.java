public class Main {
    public static void main(String[] args) {

        drukujParzystoscLiczby(50);

        for (int i = 10; i < 20; i++) {
            drukujParzystoscLiczby(i);
        }

        for (int i = 0; i < 5; i++) {
            System.out.println(czyLiczbaJestParzysta(i));
        }

        for (int i = 0; i < 100; i++) {
            boolean podzielnaI = czyJestPodzielnaPrzezLiczby(i);
            if (podzielnaI) {
                System.out.println(i + " - Podzielna!");
            }
        }

        int bezw = bezwzględna(-50);

        for (int i = -10; i < 10; i++) {
            int bezwzględna = bezwzględna(i);
            System.out.println("Wartość bezwz. z liczby " + i + " to " + bezwzględna);
        }
    }

    public static void drukujParzystoscLiczby(int liczba) {
        if (liczba % 2 == 0) {
            System.out.println("Liczba parzysta");
        } else {
            System.out.println("Liczba nieparzysta");
        }
    }

    public static boolean czyLiczbaJestParzysta(int liczba) {
//        boolean czyParzysta = (liczba % 2 == 0);
//        return czyParzysta;
        return liczba % 2 == 0;
    }

    public static boolean czyJestPodzielnaPrzezLiczby(int liczba) {
        return liczba % 3 == 0 || liczba % 5 == 0;
    }

    public static int bezwzględna(int liczba) {
        return Math.abs(liczba);
    }

    public static double odwrotna(int liczba) {
        return 1 / liczba;
    }

    public static int przeciwna(int liczba) {
        return liczba * -1;
    }

    public static double potega(int liczba) {
        return Math.pow(liczba, 5);
    }

    public static void wypiszMnożniki(int liczba) {
        for (int i = 1; i <= 10; i++) {
            System.out.println(i + " * liczba = " + (i * liczba));
        }
    }


}
